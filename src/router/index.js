import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import Alfa from '../views/Alfa.vue';
import Bravo from '../views/Bravo.vue';
import Charlie from '../views/Charlie.vue';
import Delta from '../views/Delta.vue';
import Echo from '../views/Echo.vue';
import Foxtrot from '../views/Foxtrot.vue';
import Golf from '../views/Golf.vue';
import Hotel from '../views/Hotel.vue';
import India from '../views/India.vue';
import Juliet from '../views/Juliet.vue';
import Kilo from '../views/Kilo.vue';
import Lima from '../views/Lima.vue';
import Mike from '../views/Mike.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  { path: '/Alfa', name: 'Alfa', component: Alfa },
  { path: '/Bravo', name: 'Bravo', component: Bravo },
  { path: '/Charlie', name: 'Charlie', component: Charlie },
  { path: '/Delta', name: 'Delta', component: Delta },
  { path: '/Echo', name: 'Echo', component: Echo },
  { path: '/Foxtrot', name: 'Foxtrot', component: Foxtrot },
  { path: '/Golf', name: 'Golf', component: Golf },
  { path: '/Hotel', name: 'Hotel', component: Hotel },
  { path: '/India', name: 'India', component: India },
  { path: '/Juliet', name: 'Juliet', component: Juliet },
  { path: '/Kilo', name: 'Kilo', component: Kilo },
  { path: '/Lima', name: 'Lima', component: Lima },
  { path: '/Mike', name: 'Mike', component: Mike },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
];

const router = createRouter({
  history: process.env.BASE_URL !== '/' ? createWebHashHistory() : createWebHistory('/'),
  routes,
});

export default router;
